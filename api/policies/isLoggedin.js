// module.exports = async function (req, res, proceed) {

//   // If `req.me` is set, then we know that this request originated
//   // from a logged-in user.  So we can safely proceed to the next policy--
//   // or, if this is the last policy, the relevant action.
//   // > For more about where `req.me` comes from, check out this app's
//   // > custom hook (`api/hooks/custom/index.js`).
//   console.log(JSON.stringify(req.route.path));

//   if (true) {
//     return proceed();
//   }

//   //--•
//   // Otherwise, this request did not come from a logged-in user.
//   return res.forbidden();

// };
const { verifyToken } = require('../libs/auth');
module.exports = async (req, res, next) => {
  if (req.headers.authorization) {
    const parts = req.headers.authorization.split(' ');
    if (parts.length === 2) {
      const scheme = parts[0];
      const token = parts[1];
      if (/^Bearer$/i.test(scheme)) {
        const user = await verifyToken(token);
        if (user) {
          req.user = user;
          return next();
        }
        else {
          return res.badRequest({ tokenStatus: 'incorrect', code: 401 });
        }
      } else {
        return res.badRequest({ tokenStatus: 'incorrect', code: 401 });
      }
    } else {
      return res.badRequest({ tokenStatus: 'incorrect', code: 401 });
    }
  } else {
    return res.badRequest({ tokenStatus: 'incorrect', code: 401 });
  }
};
