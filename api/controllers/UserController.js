const User = require('../models/user');
const MD5 = require('md5');
const _ = require('lodash');
const { generateToken } = require('../libs/auth');
const UserCreate = require('../schemas/UserCreate');
const Ajv = require('ajv');
var ajv = new Ajv({ allErrors: true, jsonPointers: true });

const login = async (req, res) => {
  try {
    const { username, password } = req.body;
    const userData = await User.findOne({ username, password: MD5(password) });
    if (!userData) {
      return res.badRequest({ err: 'UserName or password invalid!' })
    }
    const token = await generateToken(
      userData.username,
      userData.fullName,
    );
    res.ok({ token, fullName: userData.fullName, username: userData.username });
  } catch (err) {
    res.serverError(err);
  }
}

const create = async (req, res) => {
  try {
    console.log(req.body);
    if (!ajv.validate(UserCreate, req.body)) {
      return res.badRequest(ajv.errors);
    }
    const { username } = req.body;
    checkDuplicate = await User.findOne({
      username
    }
    );
    if (checkDuplicate) {
      return res.badRequest("User Name already exist!!!");
    }
    req.body.password = MD5(req.body.password);
    const createUser = await User.create(req.body);
    res.ok(createUser);
  } catch (err) {
    return res.serverError(err);
  }
}
const changePassword = async (req, res) => {
  try {
    const { username, password } = req.body;
    if (!username || !password) {
      throw ('No username or password entered');
    }
    const modify = await User.update({ username }, {
      password
    });
    res
      .status(200)
      .json({ code: 200, message: 'Done', data: user });
  } catch (err) {
    res
      .status(200)
      .json({ code: 500, message: 'Something is wrong!!!', error });
  }
}
const getUser = async (req, res) => {
  try {
    const page = (_.toInteger(req.param('page')) || 1) - 1;
    const page_size = _.toInteger(req.param('page_size')) || 100;
    const condition = req.allParams();
    const createdAt = req.body;
    delete condition.page;
    delete condition.page_size;
    const data = await User.find(condition).limit(page_size).skip(page * page_size).sort({ createdAt: 'descending' });
    const totalCount = await User.count();
    res.ok({ data, totalCount })
  } catch (error) {
    res.serverError(error);
  }
}
const erase = async (req, res) => {
  try {
    // if (!ajv.validate(BarcodeCreate, req.body)){
    //   return res.badRequest(ajv.errors);
    // }
    const username = req.param('name');
    matchUser = await User.findOne({
      username
    }
    );
    if (!matchUser) {
      return res.badRequest("User doesn't exist!!!");
    }
    const deleteUser = await User.findOneAndDelete(
      {
        username
      }
    );
    res.ok(deleteUser);
  } catch (err) {
    return res.serverError(err);
  }
}

const searchUser = async (req, res) => {
  try {
    const username = req.param('name');
    matchUser = await User.findOne({
      username
    }
    );
    if (!matchUser) {
      return res.badRequest("User doesn't exist!!!");
    }
    res.ok(matchUser);
  } catch (error) {
    res.serverError(error)
  }
}
const modify = async (req, res) => {
  try {
    // if (!ajv.validate(OrderSourdeCreate, req.body)){
    //   return res.badRequest(ajv.errors);
    // }
    const username = req.param('name');
    matchOrderSourceId = await User.findOne({
      username
    });
    if (!matchOrderSourceId) {
      return res.badRequest("User ID doesn't exist!!!");
    }
    if (req.body.password)
      req.body.password = md5(req.body.password);

    const modifyUser = await User.updateOne({
      username
    }, req.body);
    res.ok(modifyUser);
  } catch (err) {
    return res.serverError(err);
  }
}


module.exports = {
  create,
  changePassword,
  login,
  getUser,
  erase,
  modify,
  searchUser
}

