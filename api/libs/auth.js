const jwt = require('jsonwebtoken');
const md5 = require('md5');
const User = require('../models/user');
// const secret = require('../secret');
const _ = require('lodash');

module.exports = {
  generateToken: async function (userId, fullName) {
    const params = {
      userId, fullName
    }
    const token = jwt.sign(params,
      sails.config.datastores.secretKey, {
        expiresIn: '50h'
        // expiresIn: 1000 * 60 * 60 * 24 // expires in 24 hours
      });
    return token;

  },
  verifyToken: async function (token) {
    try {
      const decoded = jwt.verify(token, sails.config.datastores.secretKey);
      const params = {
        username: decoded.userId,
      };
      const users = await User.findOne(params);
      if (users) {
        return {
          fullName: users.fullName,
          username: users.username,
          userId: users.userId,
          role: users.role,
          token
        };
      }
      else {
        return null;
      }
    } catch (err) {
      // sails.log.error(err);
      return null;
    }
  },
};
