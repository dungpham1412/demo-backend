const mongoose = require('mongoose');
// const Joi = require('joi')
const user = mongoose.Schema({
  username : {
    type: String,
    unique: true,
    required: true
  },
  fullName: String,
  role: String,
  email:{
    type: String,
    required: true
  },
  createdAt:{
    type: Date,
    default: new Date()
  },
  password: {
    type: String,
    required: true
  }
})

// function validateUser(user) {
//   const schema = {
//       username: Joi.string().min(5).max(50).required(),
//       email: Joi.string().min(5).max(255).required().email(),
//       password: Joi.string().min(5).max(255).required()
//   };
//   return Joi.validate(user, schema);
// }

const User = mongoose.model('user',user);
module.exports = User;
// exports.validate = validateUser;

