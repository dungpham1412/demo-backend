/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions, unless overridden.       *
  * (`true` allows public access)                                            *
  *                                                                          *
  ***************************************************************************/
 'ShippingHandover/importData': ['isLoggedIn'],
 'ShippingHandover/updateProductName': ['isLoggedIn'],
 'ShippingHandover/updateShippingHandoverStatus': ['isLoggedIn'],
 'ShippingHandover/updateOutOfStock': ['isLoggedIn'],
 'ShippingHandover/updateShipping': ['isLoggedIn'],
 'ShippingHandover/deleteShippingHandover': ['isLoggedIn'],
 'Shipping/create': ['isLoggedIn'],
 
  // 103.56.157.142

};
